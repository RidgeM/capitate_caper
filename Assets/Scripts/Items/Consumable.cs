﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Consumable", menuName = "Inventory/Consumable")]
public class Consumable : Item
{
    PlayerManager playerManager;

    public override void Use()
    {
        base.Use();
        playerManager = PlayerManager.instance;
        PlayerStats playerTarget = playerManager.player.GetComponent<PlayerStats>();
        if(playerTarget != null){
            playerTarget.TakeDamage(-20);
        }
        RemoveFromInventory();
    }
}
