﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyControllerRanged : EnemyController
{
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        if(distance <= lookRadius/2){
            transform.rotation = Quaternion.LookRotation(transform.position - target.position);
            Vector3 away = transform.position + transform.forward * 5;
            agent.SetDestination(away);

            if(distance <= lookRadius){
                CharacterStats targetStats = target.GetComponent<CharacterStats>();
                if(targetStats != null){
                    combat.Attack(targetStats);
                }
            }
        }
    }
}
