﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    GameObject player;
    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Ending Game");
        player = PlayerManager.instance.player;
        if(other.gameObject == player){
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
