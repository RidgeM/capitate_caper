﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    PlayerManager playerManager;
    void Start()
    {
        playerManager = PlayerManager.instance;
    }
    public void OnTriggerEnter(Collider other)
    {
        PlayerStats playerTarget = other.GetComponent<PlayerStats>();
        if(playerTarget != null){
            playerTarget.TakeDamage(20);
        }
    }
}
